# Script to create Citadel orders.

## How to:

npm i

node createData.ts -c **number** -v -d **number**

- The -c (--count) parameter indicates the amount of orders to be created. The -v (--verify) indicates the state of the verify parameter for each dispense.
- When the -c parameter is not sent by default will creates one order.
- When the -v parameter is not sent by default no dispense needs verification process
- When the -d parameter is not sent by default will create one dispense for the order.

## TODO:

- Load Rest URL from Env.
- Load Preparer from Env.
- Load EndUser from Env.
- Receive end unser and preparer from params.
- Move Schedule and dispense creation to a proper class.
