const faker = require('faker');
const axios = require('axios');
const { Command } = require('commander');
const program = new Command();
program
  .option('-c, --count <count>')
  .option('-v, --verify')
  .option('-d, --dispenses <count>');

let success = 0;
const resultData = {};


const createSchedule = () => {
  const name = faker.name.findName();
  const sch = {};
  sch['scheduleName'] = name;
  sch['status'] = 'OPEN';
  sch['preparer'] = 'preparer-dd9ea9ae-aa64-4c2c-906c-333bf9f494fd';
  sch['enduser'] = 'enduser-819dd91b-f9db-4685-88f4-c544b2e29f2f';
  sch['createDateTime'] = new Date().getTime();
  sch['expireDateTime'] = new Date(faker.date.future()).getTime();
  sch['flowId'] = 'flow-schedule-default';
  sch['id'] = `schedule-${generateCode()}`;
  sch['clientCode'] = generateCode();
  sch['undoCode'] = generateCode();
  sch['loadCode'] = generateCode();
  sch['reclaimCode'] = generateCode();
  sch['reclaimedCode'] = generateCode();
  sch['verifyCode'] = generateCode();
  sch['meta'] = { ljid: 'testID' };
  // sch['tags'] = [{ name: 'BIG', required: true }]

  return sch;
}

const createDispense = (schId, dispSchName, verify) => {
  const disp = {};
  disp['id'] = `dispense-${generateCode()}`;
  disp['dispenseSchName'] = dispSchName;
  disp['verify'] = verify;
  disp['product'] = [faker.commerce.productName()];
  disp['schedule'] = schId;
  disp['createTimeDsp'] = new Date().getTime();
  disp['expireTimeDsp'] = new Date(faker.date.future()).getTime();
  disp['loadCode'] = generateCode();
  disp['undoCode'] = generateCode();
  disp['reclaimCode'] = generateCode();
  disp['reclaimedCode'] = generateCode();
  disp['verifyCode'] = generateCode();
  disp['dispensePrice'] = faker.commerce.price();
  disp['minTemperature'] = 10;
  disp['maxTemperature'] = 30;
  disp['minPenaltyFactor'] = 5;
  disp['maxPenalty'] = '5';
  disp['flowId'] = 'flow-dispense-default';
  return disp;
}

const generateCode = () => {
  return faker.random.number({ min: 100000, max: 999999 }).toString();
}


const init = async () => {
  program.parse(process.argv);
  const options = program.opts();
  const amount = options.count || 1;
  const verify = options.verify;
  const dispenses = options.dispenses || 1;
  await saveSchedule(0, amount, verify, dispenses);
};


const saveSchedule = async (count, total, verify, dispAmount) => {
  if (count < total) {
    setTimeout(async () => {
      console.log('************** NEW DATA SET **************');

      const schedule = createSchedule();
      console.log('Creating schedule...');
      let scheduleId = schedule['id'];
      resultData[scheduleId] = {};
      const dispenses = [];
      let dispCount = dispAmount;
      for (let i = 0; i < dispCount; i++) {
        console.log(i)
        let dispense = createDispense(schedule['id'], schedule['scheduleName'], verify);
        dispenses.push(dispense)
        resultData[scheduleId][i.toString()] = dispense['id'];
      }
      schedule['dispenses'] = dispenses;
      await axios
        .post('http://localhost:3001/schedules', schedule)
        .then(async res => {
          console.log('Schedule created...', schedule);
          success += 1;
          console.log('Saved: ', success);
          count += 1;
          await saveSchedule(count, total, verify, dispAmount);
        })
        .catch(async error => {
          console.error(error)
          await saveSchedule(count, total, verify, dispAmount);
        })
    }, 1000)
  } else {
    console.table(resultData);
  }
}



init();